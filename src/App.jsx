import React, { useEffect, useState } from "react";
import { Routes, Route, Link } from "react-router-dom";
import axios from "axios";
import Header from "./components/header/Header";
import ProductList from "./components/ProductList/ProductList";
import Cart from "./components/Cart/Cart";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import modalTemplates from "./components/modal/modalTemplates.js";
import Favorites from "./components/favorites/Favorites";

let modalDeclaration = {};

const [modalWindowDeclarations] = Object.values(modalTemplates);
console.log(modalWindowDeclarations);

// const App = () => {
// const [cards, setCards] = useState([]);
// const [displayState, setDisplayState] = useState("none");
// const [classWrapper, setClassWrapper] = useState("");
// const [modalWindow, setModalWindowDeclarations] = useState(modalWindowDeclarations);
// const [classNameIcon, setClassNameIcon] = useState("favor-icon");
// let [productsList, setProductsList] = useState([]);
// const [favoritesList, setFavoritesList] = useState([]);
// const [targetProd, setTargetProd] = useState()

// useEffect(() => {
//   axios("/airplane.json").then((res) => {
//     setCards(res.data)
//   });
//   if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
//   if (!localStorage.getItem("favorites"))
//     localStorage.setItem("favorites", "[]");
// }, []);

// useEffect(() => {
//   setFavoritesList(JSON.parse(localStorage.getItem("favorites")))
// }, [ ]);

// useEffect(() => {
//     productsList = cards.map((element) => {
//     favoritesList.includes(element.id)
//       ? (element.isFavorites = true)
//       : (element.isFavorites = false);
//     return element;
//   });

// }, [ ]);

// const didMount = () => {
//   setClassWrapper("wrapper")

//   };

//   const didUnmounted = () => {
//     setClassWrapper("")

//   };

//   const closeModal = (e) => {
//     if (e.target.classList.contains("wrapper")) {
//       setDisplayState("none")

//     }
//   };

//   const openModal = (e) => {
//     const modalID = e.target.dataset.modalId;
//     modalDeclaration = modalWindow.find(
//       (item) => item.id === modalID
//     );
//     console.log(e.target.id);

//     //let targetProd = e.target.id;
//     setTargetProd(e.target.id)
//     setDisplayState("open")

//   };

//   // const closeModal = () => {
//   //   setDisplayState("none")

//   // };

//   const addToCart = (e) => {
//     setDisplayState("none")
//     // console.log(e.target);

//     let cartProds = JSON.parse(localStorage.getItem("cart"));
//     let newProds = cartProds.map((element) => element);
//     newProds.push(targetProd);
//     localStorage.setItem("cart", JSON.stringify(newProds));
//   };

//   const addToFavorites = (id) => {
//     const favoritesProducts = JSON.parse(localStorage.getItem("favorites"));
//     let newProducts = favoritesProducts.map((id) => id);
//     if (newProducts.includes(id)) {
//       newProducts = newProducts.filter((item) => item !== id);
//       localStorage.setItem("favorites", JSON.stringify(newProducts));
//     } else {
//       newProducts.push(id);
//       localStorage.setItem("favorites", JSON.stringify(newProducts));
//     }
//     setFavoritesList(newProducts)

//   };

//     // favoritesList = JSON.parse(localStorage.getItem("favorites"));
//     // productsList = cards.map((element) => {
//     //   favoritesList.includes(element.id)
//     //     ? (element.isFavorites = true)
//     //     : (element.isFavorites = false);
//     //   return element;
//     // });

//     const {
//       className,
//       id,
//       header,
//       closeButton,
//       description,
//       classNameButton,
//       textButtonLeft,
//       textButtonRight,
//     } = modalDeclaration;

//     //cards = productsList;

//   return (
//     <>
//         <div
//           className={classWrapper}
//           onClick={closeModal}
//         ></div>
//         <Header />

//         <ProductList
//           products={cards}
//           onClick={openModal}
//           favorites={addToFavorites}
//           classNameIcon={classNameIcon}
//         />

//         {displayState === "open" && (
//           <Modal
//             className={className}
//             id={id}
//             onClick={closeModal}
//             show={didMount}
//             hidden={didUnmounted}
//             header={header}
//             closeButton={closeButton}
//             description={description}
//             actions={
//               <>
//                 <Button
//                   className={classNameButton}
//                   text={textButtonLeft}
//                   onClick={addToCart}
//                 />
//                 <Button
//                   className={classNameButton}
//                   text={textButtonRight}
//                   onClick={closeModal}
//                 />
//               </>
//             }
//           />
//         )}
//       </>
//   );
// };

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      displayState: "none",
      classWrapper: "",
      modalWindowDeclarations: modalWindowDeclarations,
      classNameIcon: "favor-icon",
      productsList: [],
      favoritesList: [],
    };
  }

  componentDidMount() {
    axios("/airplane.json").then((res) => {
      this.setState({ cards: res.data });
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }

  didMount = () => {
    this.setState({
      classWrapper: "wrapper",
    });
  };

  didUnmounted = () => {
    this.setState({
      classWrapper: "",
    });
  };

  closeModal = (e) => {
    if (e.target.classList.contains("wrapper")) {
      this.setState({
        displayState: "none",
      });
    }
  };

  openModal = (e) => {
    const modalID = e.target.dataset.modalId;
    modalDeclaration = this.state.modalWindowDeclarations.find(
      (item) => item.id === modalID
    );
    this.targetProd = e.target.id;
    this.setState({
      displayState: "open",
    });
  };

  closeModal = () => {
    this.setState({
      displayState: "none",
    });
  };

  // addToCart = (e) => {
  //   this.setState({
  //     displayState: "none",
  //   });
  //   let cartProds = JSON.parse(localStorage.getItem("cart"));
  //   let newProds = cartProds.map((element) => element);
  //   newProds.push(this.targetProd);
  //   localStorage.setItem("cart", JSON.stringify(newProds));
  // };

  onClickModalBtn = (e) => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newProds = cartProds.map((element) => element);
    if (e.target.dataset.modalId === "modalID2") {
      newProds.push(this.targetProd);
      localStorage.setItem("cart", JSON.stringify(newProds));
    }
    if (e.target.dataset.modalId === "modalID3") {
      newProds = cartProds.map((element) => element);
      if (newProds.includes(this.targetProd)) {
        newProds = newProds.filter((item) => item !== this.targetProd);
        localStorage.setItem("cart", JSON.stringify(newProds));
      }
    }
    this.setState({
      displayState: "none",
    });
  };

  addToFavorites = (id) => {
    const favoritesProducts = JSON.parse(localStorage.getItem("favorites"));
    let newProducts = favoritesProducts.map((id) => id);
    if (newProducts.includes(id)) {
      newProducts = newProducts.filter((item) => item !== id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    } else {
      newProducts.push(id);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    }
    this.setState({
      favoritesList: newProducts,
    });
  };

  render() {
    const favoritesList = JSON.parse(localStorage.getItem("favorites"));
    const productsList = this.state.cards.map((element) => {
      favoritesList.includes(element.id)
        ? (element.isFavorites = true)
        : (element.isFavorites = false);
      return element;
    });

    const {
      className,
      id,
      header,
      closeButton,
      description,
      classNameButton,
      textButtonLeft,
      textButtonRight,
    } = modalDeclaration;

    const cards = productsList;

    return (
      <>
        <div
          className={this.state.classWrapper}
          onClick={this.closeModal}
        ></div>
        <Header />
        <div className="content">

        
        <Routes>
        
          <Route path="/" element={<ProductList products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={true}/>}></Route>
          <Route path="cart" element={<Cart products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={false}/>}></Route>
          <Route path="favorites" element={<Favorites products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={true}/>}></Route>




          
        </Routes>
        </div>
        

        {/* <ProductList
          products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={true}
        /> */}

        {this.state.displayState === "open" && (
          <Modal
            className={className}
            id={id}
            onClick={this.closeModal}
            show={this.didMount}
            hidden={this.didUnmounted}
            header={header}
            closeButton={closeButton}
            description={description}
            actions={
              <>
                <Button
                  id={id}
                  className={classNameButton}
                  dataModal={id}
                  text={textButtonLeft}
                  onClick={this.onClickModalBtn}
                />
                <Button
                  className={classNameButton}
                  text={textButtonRight}
                  onClick={this.closeModal}
                />
              </>
            }
          />
        )}
        {/* <Cart
          products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={false}
        /> */}
        {/* <Favorites
          products={cards}
          onClick={this.openModal}
          favorites={this.addToFavorites}
          classNameIcon={this.state.classNameIcon}
          closeButtonAddCart={true}
        /> */}
      </>
    );
  }
}

export default App;
