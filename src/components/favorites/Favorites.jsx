import React, { Component } from "react";
import ProductList from "../ProductList/ProductList";
class Favorites extends Component {
  render() {
    let favoritesProdsId = JSON.parse(localStorage.getItem("favorites"));

    const {
      products,
      onClick,
      favorites,
      classNameIcon,
      removeToCart,
      closeButtonAddCart,
    } = this.props;

    const favoritesProducts = filterBy(products, favoritesProdsId);
    function filterBy(a, b) {
      let typedArr = a.filter(function (a) {
        return a.id === b.find((item) => item === a.id);
      });
      return typedArr;
    }

    return (
      <>
        <h2>YOUR FAVORITES</h2>
        <ProductList
          products={favoritesProducts}
          onClick={onClick}
          favorites={favorites}
          classNameIcon={classNameIcon}
          closeButtonAddCart={closeButtonAddCart}
        />
      </>
    );
  }
}

export default Favorites;
