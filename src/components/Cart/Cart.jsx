import React, { Component } from "react";
import { Link } from "react-router-dom";
import ProductList from "../ProductList/ProductList";
class Cart extends Component {
  render() {
    let cartProdsId = JSON.parse(localStorage.getItem("cart"));

    const {
      products,
      onClick,
      favorites,
      classNameIcon,
      removeToCart,
      closeButtonAddCart,
    } = this.props;

    const cartProducts = filterBy(products, cartProdsId);
    function filterBy(a, b) {
      let typedArr = a.filter(function (a) {
        return a.id === b.find((item) => item === a.id); 
      });
      return typedArr;
    }

    

    return (
      <>
        <h2>YOUR CART</h2>
        <ProductList
          products={cartProducts}
          onClick={onClick}
          favorites={favorites}
          classNameIcon={classNameIcon}
          closeButtonAddCart={closeButtonAddCart}
        />
      </>
    );
  }
}

export default Cart;
