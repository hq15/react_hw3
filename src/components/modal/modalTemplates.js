const modalTemplates = [
  {
    id: "modalID1",
    className: "modal modal__warning",
    header: "Do you want to add a product?",
    description:
      "Lorem res nesciunt ipsam quidem beatae animi molestias illo aperiam possimus a sapiente nam? Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis odit magni obcaecati atque sapiente suscipit modi, architecto, nobis illo cumque nesciunt dolores voluptatem, sint labore! Distinctio delectus quibusdam soluta est!",
    classNameButton: "modal__btn",
    textButtonLeft: "Ok",
    textButtonRight: "Cancel",
    closeButton: true,
    buttonAction() {
      alert("The file has been deleted");
    },
  },
  {
    id: "modalID2",
    className: "modal modal__info",
    header: "Do you want to add to cart this product?",
    description:
      "The item will be added to the cart",
    classNameButton: "modal__btn",
    textButtonLeft: "Add",
    textButtonRight: "Cancel",
    closeButton: false,
    buttonAction() {
      console.log("This file has been uploaded");
    },
  },
  {
    id: "modalID3",
    className: "modal modal__add-to-card",
    header: "You want to remove this product from your cart?",
    description:
      "The item will be removed from the cart",
    classNameButton: "modal__btn",
    textButtonLeft: "Remove",
    textButtonRight: "Cancel",
    closeButton: false,
    buttonAction() {
      console.log("This file has been uploaded");
    },
  },

];

// eslint-disable-next-line
export default { modalTemplates };
