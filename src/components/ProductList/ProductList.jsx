import React, { Component } from "react";
import PropTypes from "prop-types";
import CardProduct from "../CardProduct/CardProduct";
import "./ProductList.scss";

class ProductList extends Component {
  render() {
    const {
      products,
      onClick,
      favorites,
      classNameIcon,
      removeToCart,
      closeButtonAddCart,
      className,
    } = this.props;

    const prodCards = products.map((e) => (
      
        <CardProduct
          key={e.id}
          card={e}
          className={className}
          onClick={onClick}
          favorites={favorites}
          classNameIcon={classNameIcon}
          removeToCart={removeToCart}
          closeButtonAddCart={closeButtonAddCart}
        />
      
    ));

    return <div className="products-wrapper">{prodCards}</div>;
  }
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ).isRequired,
  onClick: PropTypes.func,
  favorites: PropTypes.func,
  closeButtonAddCart: PropTypes.bool,
};

ProductList.defaultProps = {
  onClick: null,
  favorites: null,
  closeButtonAddCart: true,
};

export default ProductList;
